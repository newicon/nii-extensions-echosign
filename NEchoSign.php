<?php

include('echosignapi/Autoloader.php');

class NEchoSign extends CApplicationComponent
{
  public $wsdl      = null;
  public $api_key   = null;

  protected $client = null;
  protected $api    = null;

  /**
   * Implementation of CApplication init() function
   * Loads the echosign api code
   * @return null
   */
  public function init()
  {
    // Load the API Code
    $ESLoader = new SplClassLoader('EchoSign', realpath(__DIR__.'/echosignapi/lib').'/');
    $ESLoader->register();
  }

  /**
   * Simple EchoSign sign function
   * @param string $filePath - the path to the file to be signed
   * @param string $recipientEmail - the email recipient(s)
   * @param [optional] string - the callback URL for EchoSign to call once
   *        once the document is signed
   * @return string key - the echosign unique key for the document
   */
  public function sign($name,$filePath,$recipientEmail,$callbackUrl=null)
  {
    // Connect to the API
    $this->connect();

    // Create the file information to send
    $file = EchoSign\Info\FileInfo::createFromFile($filePath);

    // Create the recipient email information
    $recipients = new EchoSign\Info\RecipientInfo;
    if(is_array($recipientEmail))
      $recipients->addRecipients($recipientEmail);
    else
      $recipients->addRecipient($recipientEmail);

    // Create the document information, and append the emails
    $document = new EchoSign\Info\DocumentCreationInfo($name, $file);
    $document->setRecipients($recipients);

    if(isset($callbackUrl))
      $document->setCallbackInfo($callbackUrl);

    $result = null;
    try{
      $result = $this->api->sendDocument($document);
    }catch(Exception $e){
      throw $e;
    }
    $key = $result->documentKeys->DocumentKey->documentKey;
    return $key;
  }

  public function widget($name,$filePath,$recipientEmail,$options = array())
  {
    $this->connect();

    // Create the file information to send
    $file = EchoSign\Info\FileInfo::createFromFile($filePath);

    $widget = new EchoSign\Info\WidgetCreationInfo($name, $file);
    $personalization = new EchoSign\Info\WidgetPersonalizationInfo($recipientEmail);   

    if(!empty($options))
    {
      if(isset($options['completionUrl']))
      {
        $widget->setWidgetCompletionInfo($options['completionUrl']);
      }
    }

    try{
      $result = $this->api->createPersonalEmbeddedWidget($widget,$personalization);
    }catch(Exception $e){
    }

    $embeddedResult = $result->embeddedWidgetCreationResult;

    return array('javascript' 	=> $embeddedResult->javascript,
                 'documentKey'	=> $embeddedResult->documentKey,
		 'errorMessage' => $embeddedResult->errorMessage,
		 'errorCode'    => $embeddedResult->errorCode,
		 'success'      => $embeddedResult->success);
  }

  /**
   * Simple function to return the pdf associated with a particular
   * document key
   * @param string key - the document key as returned by sign()
   * @return data - base64 encoded version of the PDF, or NULL if error
   */
  public function getBytes($documentKey)
  {
    $this->connect();
    $options = new EchoSign\Options\GetDocumentsOptions;
    $result = $this->api->getDocuments($documentKey,$options);
    return $result->getDocumentsResult->documents->DocumentContent->bytes;
  }

  /**
   * Connect to the EchoSign API using a SoapClient
   * @return null
   */
  protected function connect()
  {
    $this->client = new SoapClient(EchoSign\API::getWSDL());
    $this->api = new EchoSign\API($this->client,$this->api_key);
  }

  /**
   * Connect and expose the Echosign API
   * @return \EchoSign\Api
   */
  public function api()
  {
    $this->connect();
    return $this->api;
  }
}



